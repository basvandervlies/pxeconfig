
PXE-menu tools README file
--------------------------

Author: Ole Holm Nielsen
        Dept. of Physics, Technical University of Denmark
        E-mail: Ole.H.Nielsen@fysik.dtu.dk

Version: 1.2
Date: 15-Oct-2010

Summary
-------

The PXE-menu tools are used to control the booting of networked computers
directly from the computer's console at the BIOS level before any operating
system has been loaded.  This is extremely useful for diagnostic purposes,
or for selecting an operating system installation method, of an individual
computer.  If you need to do the same operation on many computers, it is
better to use the pxeconfig tools (https://subtrac.sara.nl/oss/pxeconfig)
to control the booting from your network's DHCP/TFTP server.

Additional information
----------------------

We provide further details on our Linux cluster's homepage:
https://wiki.fysik.dtu.dk/niflheim/PXE-booting

Prerequisite software
---------------------

We use some of the SYSLINUX tools for providing the PXE menus in this package, see
http://syslinux.zytor.com/pxe.php and http://syslinux.zytor.com/menu.php.
For your convenience the binary files are included with this package, but you can
also build them yourself by following the instructions in the 
https://wiki.fysik.dtu.dk/niflheim/PXE-booting web-page.

What's included
---------------

The pxemenu/ directory tree should be copied to your DHCP/TFTP server's 
/tftpboot directory (or wherever your TFTP server has its root directory).
Please see the file INSTALL-pxemenu for detailed installation instructions.

When your computer has been configured to do a PXE boot, it will automatically
download the file pxelinux.0 by TFTP and execute it, and pxelinux.0 will
download configuration files from the pxelinux.cfg/ subdirectory. 
In the pxelinux.cfg/* configuration files are references to files that
should be downloaded by TFTP, and these files are all relative to the
top-level TFTP directory.

Here we assume that the file named "default" in pxelinux.cfg/ is a soft-link to
the default.menu file so that the PXE menus will be loaded.  You could also let 
default.menu be one of the options which you configure by using pxeconfig.

The most important files included in the pxemenu/ directory are:

  pxelinux.0 memdisk Binary files from the SYSLINUX 4.02 which are needed
         for the usual booting methods.
  com32/*.c32: A few SYSLINUX 4.02 COM32 files, see the SYSLINUX source doc/comboot.txt.
  pxelinux.cfg/default.menu: The main PXE menu configuration file which 
         loads all the other PXE menus listed below.
         The default.menu has a timeout of 5 seconds configured so that you
         may use the arrow buttons to select options; otherwise the default
         boot from harddisk will be selected.  This is most likely what you want
         to do on a Linux cluster for unattended operation, but for other uses
         the timeout can be commented out so that the menus will wait forever.

Example PXE menu files
----------------------

In the pxemenu/ directory are a number of examples that you may find useful
for customizing your own PXE menu configurations.  They are:

  x3455/: Example of PXE menus for an IBM X-series x3455 server.
  mysystem/: Example of PXE menus for a generic server.
          Copy this example and modify it for your particular server.
          IMPORTANT: Always keep all images related to one type of hardware
          in the same subdirectory so that you don't mix up different hardwares !
  Tools/: Example of some diagnostics tools, including Memtest86.
          The vendor disk diagnostics tools you must download yourself
          (usually copyrighted), see links in tools.conf.
  centos.conf: Start a CentOS Linux installation.

SystemImager installation with PXE menus
----------------------------------------

SystemImager installation can be started as shown in the x3455/x3455.conf
example file.  Each type of hardware is supported in SystemImager by a
specially generated UYOK (Use Your Own Kernel) vmlinux kernel and
initrd.img RAM-disk, see http://wiki.systemimager.org/index.php/UYOK.

Therefore you must copy kernel and initrd.img from
/var/lib/systemimager/images/<IMAGENAME>/etc/systemimager/boot/
to the subdirectory for your hardware (/tftpboot/x3455/ in the present example).


What you must supply yourself
-----------------------------

The simple PXE menu tools included in this package are just simple but useful 
examples that have proven useful in our Linux cluster and desktop environment.
You should provide boot images for the tools that you want to run, such as
vendor firmware upgrade diskettes or Linux installation kernels, for example.

If you want to add a physical DOS boot diskette of your own to the PXE menus,
you can create an image of the physical diskette by:

  dd if=/dev/fd0 of=myimage.img

(assuming that the diskette drive on your Linux PC is /dev/fd0).

Special hardware issues
-----------------------

Broadcom network adapters:  It is known that certain Broadcom Ethernet
adapters are buggy.  If your system hangs while you move up and down the
PXE menus, then you've probably been hit by this.  Please upgrade the
firmware on the Broadcom adapter to the latest release. 

In the case of IBM X-series servers, you MUST upgrade the Broadcom firmware
to at least release 2.0.0.
